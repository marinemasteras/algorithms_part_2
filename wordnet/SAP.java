/* *****************************************************************************
 *  Name:
 *  Date:
 *  Description:
 **************************************************************************** */

import edu.princeton.cs.algs4.BreadthFirstDirectedPaths;
import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.StdOut;

public class SAP {

    private final Digraph digraph;

    // constructor takes a digraph (not necessarily a DAG)
    public SAP(Digraph G) {
        this.digraph = new Digraph(G);
    }

    // length of shortest ancestral path between v and w; -1 if no such path
    public int length(int v, int w) {
        BreadthFirstDirectedPaths bfsV = new BreadthFirstDirectedPaths(digraph, v);
        ExtendedBreadthFirstDirectedPaths bfsW = new ExtendedBreadthFirstDirectedPaths(digraph,
                                                                                       w, bfsV);
        return bfsW.distance;
    }

    // a common ancestor of v and w that participates in a shortest ancestral path; -1 if no such path
    public int ancestor(int v, int w) {
        BreadthFirstDirectedPaths bfsV = new BreadthFirstDirectedPaths(digraph, v);
        ExtendedBreadthFirstDirectedPaths bfsW = new ExtendedBreadthFirstDirectedPaths(digraph, w,
                                                                                       bfsV);
        return bfsW.synIndex;
    }

    // length of shortest ancestral path between any vertex in v and any vertex in w; -1 if no such path
    public int length(Iterable<Integer> v, Iterable<Integer> w) {
        if (v != null && !v.iterator().hasNext() || w != null && !w.iterator().hasNext()) {
            return -1;
        }
        BreadthFirstDirectedPaths bfsV = new BreadthFirstDirectedPaths(digraph, v);
        ExtendedBreadthFirstDirectedPaths bfsW = new ExtendedBreadthFirstDirectedPaths(digraph, w,
                                                                                       bfsV);
        return bfsW.distance;
    }

    // a common ancestor that participates in shortest ancestral path; -1 if no such path
    public int ancestor(Iterable<Integer> v, Iterable<Integer> w) {
        if (v != null && !v.iterator().hasNext() || w != null && !w.iterator().hasNext()) {
            return -1;
        }
        BreadthFirstDirectedPaths bfsV = new BreadthFirstDirectedPaths(digraph, v);
        ExtendedBreadthFirstDirectedPaths bfsW = new ExtendedBreadthFirstDirectedPaths(digraph, w,
                                                                                       bfsV);
        return bfsW.synIndex;
    }

    // do unit testing of this class
    public static void main(String[] args) {
        In in = new In(args[0]);
        Digraph G = new Digraph(in);
        SAP sap = new SAP(G);
        // int v = Integer.parseInt(args[1]);
        int v = 9;
        // int w = Integer.parseInt(args[2]);
        int w = 12;
        int length = sap.length(v, w);
        int ancestor = sap.ancestor(v, w);
        StdOut.printf("length = %d, ancestor = %d\n", length, ancestor);
    }

    private class ExtendedBreadthFirstDirectedPaths {
        private static final int INFINITY = Integer.MAX_VALUE;
        private boolean[] marked;  // marked[v] = is there an s->v path?
        private int[] distTo;      // distTo[v] = length of shortest s->v path
        private int synIndex = -1;
        private int distance;

        public ExtendedBreadthFirstDirectedPaths(Digraph G, Iterable<Integer> sources,
                                                 BreadthFirstDirectedPaths bfsB) {
            marked = new boolean[G.V()];
            distTo = new int[G.V()];
            distance = INFINITY;
            for (int v = 0; v < G.V(); v++)
                distTo[v] = INFINITY;
            validateVertices(sources);
            bfs(G, sources, bfsB);
        }

        public ExtendedBreadthFirstDirectedPaths(Digraph G, int s,
                                                 BreadthFirstDirectedPaths bfsB) {
            marked = new boolean[G.V()];
            distTo = new int[G.V()];
            distance = INFINITY;
            for (int v = 0; v < G.V(); v++)
                distTo[v] = INFINITY;
            validateVertex(s);
            bfs(G, s, bfsB);
        }

        // BFS from single source
        private void bfs(Digraph G, int s, BreadthFirstDirectedPaths bfsB) {
            Queue<Integer> q = new Queue<Integer>();
            distance = INFINITY;
            marked[s] = true;
            distTo[s] = 0;
            q.enqueue(s);
            bfsLoop(G, bfsB, q);
        }

        // BFS from multiple sources
        private void bfs(Digraph G, Iterable<Integer> sources, BreadthFirstDirectedPaths bfsB) {
            Queue<Integer> q = new Queue<Integer>();
            distance = INFINITY;
            for (int s : sources) {
                marked[s] = true;
                distTo[s] = 0;
                q.enqueue(s);
            }
            bfsLoop(G, bfsB, q);
        }

        private void bfsLoop(Digraph G, BreadthFirstDirectedPaths bfsB, Queue<Integer> q) {
            while (!q.isEmpty()) {
                int v = q.dequeue();
                recalculateDistance(bfsB, v);
                for (int w : G.adj(v)) {
                    if (!marked[w]) {
                        distTo[w] = distTo[v] + 1;
                        marked[w] = true;
                        recalculateDistance(bfsB, w);
                        q.enqueue(w);
                    }
                }
            }
            if (distance > G.V()) {
                distance = -1;
            }
        }

        private void recalculateDistance(BreadthFirstDirectedPaths bfsB, int v) {
            int distanceSum;
            if (bfsB.hasPathTo(v)) {
                distanceSum = bfsB.distTo(v) + distTo[v];
                if (distanceSum < distance) {
                    synIndex = v;
                    distance = distanceSum;
                }
            }
        }

        // throw an IllegalArgumentException unless {@code 0 <= v < V}
        private void validateVertex(int v) {
            int vertex = marked.length;
            if (v < 0 || v >= vertex)
                throw new IllegalArgumentException(
                        "vertex " + v + " is not between 0 and " + (vertex - 1));
        }

        // throw an IllegalArgumentException unless {@code 0 <= v < V}
        private void validateVertices(Iterable<Integer> vertices) {
            if (vertices == null) {
                throw new IllegalArgumentException("argument is null");
            }
            for (Integer v : vertices) {
                if (v == null) {
                    throw new IllegalArgumentException("vertex is null");
                }
                validateVertex(v);
            }
        }
    }
}

