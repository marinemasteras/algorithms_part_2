/*
  *****************************************************************************
  *  Name:
  *  Date:
  *  Description:
  ****************************************************************************
  */

 import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.Topological;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

 public class WordNet {

     private final Map<String, List<Integer>> nounMap;
     private final List<String> synsets;
     private final Digraph digraph;
     private final SAP sap;

     // constructor takes the name of the two input files
     public WordNet(String synsets, String hypernyms) {
         if (synsets == null || hypernyms == null) {
             throw new IllegalArgumentException();
         }

         this.nounMap = new HashMap<String, List<Integer>>();
         this.synsets = new ArrayList<>();
         In in = new In(synsets);
         parseSynsetsToNounMap(in);
         this.digraph = new Digraph(this.synsets.size());
         in = new In(hypernyms);
         parseHypernymsToRootedDAG(in);
         this.sap = new SAP(digraph);
         isRootedDAG();
     }

     private void isRootedDAG() {
         Topological tp = new Topological(digraph);
         if (!tp.hasOrder()) {
             throw new IllegalArgumentException();
         }
     }

     private void parseSynsetsToNounMap(In in) {
             String[] singleSynset = in.readLine().split(",");
             String[] nounsFromSynset = singleSynset[1].split(" ");
             for (String noun : nounsFromSynset) {
                 if (!nounMap.containsKey(noun)) {
                     nounMap.put(noun, new LinkedList<>());
                 }
                 nounMap.get(noun).add(Integer.valueOf(singleSynset[0]));
             }
             this.synsets.add(singleSynset[1]);
         }


     private void parseHypernymsToRootedDAG(In in) {
         while (in.hasNextLine()) {
             String[] singleHypernym = in.readLine().split(",");
             int v = Integer.parseInt(singleHypernym[0]);

             for (int i = 1; i < singleHypernym.length; i++) {
                 int outdegreeIndex = Integer.parseInt(singleHypernym[i]);
                 digraph.addEdge(v, outdegreeIndex);
             }
         }
     }


     // returns all WordNet nouns
     public Iterable<String> nouns() {
         return nounMap.keySet();
     }

     // is the word a WordNet noun?
     public boolean isNoun(String word) {
         if (word == null) {
             throw new IllegalArgumentException();
         }
         return nounMap.containsKey(word);
     }

     // distance between nounA and nounB (defined below)
     public int distance(String nounA, String nounB) {
         if (!isNoun(nounA) || !isNoun(nounB))
             throw new IllegalArgumentException();
         List<Integer> as = nounMap.get(nounA);
         List<Integer> bs = nounMap.get(nounB);
         return sap.length(as, bs);
     }

     // a synset (second field of synsets.txt) that is the common ancestor of nounA and nounB
     // in a shortest ancestral path (defined below)
     public String sap(String nounA, String nounB) {
         if (!isNoun(nounA) || !isNoun(nounB))
             throw new IllegalArgumentException();
         List<Integer> as = nounMap.get(nounA);
         List<Integer> bs = nounMap.get(nounB);
         return synsets.get(sap.ancestor(as, bs));
     }


     // do unit testing of this class
     public static void main(String[] args) {

         // WordNet w = new WordNet("synsets15.txt", "hypernyms15Tree.txt");
         // WordNet w = new WordNet("synsets500-subgraph.txt", "hypernyms500-subgraph.txt");
         // WordNet w = new WordNet("synsets50000-subgraph.txt", "hypernyms50000-subgraph.txt");
         WordNet w = new WordNet("synsets3.txt", "hypernyms3InvalidTwoRoots.txt");

         System.out.println(w.distance("ABO_antibodies", "Adzhar"));
         // System.out.println(w.distance("tetanus_immunoglobulin", "reagin"));
         // System.out.println(w.sap("tetanus_immunoglobulin", "reagin"));
         System.out.println(w.sap("ABO_antibodies", "Adzhar"));
     }
 }
