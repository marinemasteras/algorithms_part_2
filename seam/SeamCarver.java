/* *****************************************************************************
 *  Name:
 *  Date:
 *  Description:
 **************************************************************************** */

import edu.princeton.cs.algs4.Picture;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class SeamCarver {

    private Picture picture;
    private boolean isTransposed;


    // create a seam carver object based on the given picture
    public SeamCarver(Picture picture) {
        if (picture == null) {
            throw new IllegalArgumentException();
        }
        this.picture = new Picture(picture);
        this.isTransposed = false;
    }

    // current picture
    public Picture picture() {
        return new Picture(this.picture);
    }

    // width of current picture
    public int width() {
        return this.picture.width();
    }

    // height of current picture
    public int height() {
        return this.picture.height();
    }

    // energy of pixel at column x and row y
    public double energy(int x, int y) {
        if (x < 0 || x > width() - 1 || y < 0 || y > height() - 1) {
            throw new IllegalArgumentException();
        }
        return (x > 0 && x < picture.width() - 1) && y > 0 && y < picture.height() - 1 ?
               Math.sqrt(calculateDx(x, y) + calculateDy(x, y)) : 1000;
    }

    private double calculateDx(int x, int y) {
        Color c1 = picture.get(x + 1, y);
        Color c2 = picture.get(x - 1, y);
        double r = Math.pow(c1.getRed() - c2.getRed(), 2);
        double g = Math.pow(c1.getGreen() - c2.getGreen(), 2);
        double b = Math.pow(c1.getBlue() - c2.getBlue(), 2);
        return r + g + b;
    }

    private double calculateDy(int x, int y) {
        Color c1 = picture.get(x, y + 1);
        Color c2 = picture.get(x, y - 1);
        double r = Math.pow(c1.getRed() - c2.getRed(), 2);
        double g = Math.pow(c1.getGreen() - c2.getGreen(), 2);
        double b = Math.pow(c1.getBlue() - c2.getBlue(), 2);
        return r + g + b;
    }

    // sequence of indices for horizontal seam
    public int[] findHorizontalSeam() {
        double[][] energyTo = new double[width()][height()];
        Pixel[][] pixelFrom = new Pixel[width()][height()];

        setInitialEnergies(energyTo);
        for (int x = 0; x < picture.width(); x++) {
            for (int y = 0; y < picture.height(); y++) {
                setPixelsFrom(energyTo, pixelFrom, x, y);
                updateEnergiesWithEnergiesTo(energyTo, pixelFrom, x, y);
            }
        }

        Pixel lastPixel = getLastPixel(energyTo, pixelFrom);
        return getSeam(lastPixel, pixelFrom);
    }

    private void setInitialEnergies(double[][] energyTo) {
        for (int x = 0; x < picture.width(); x++) {
            for (int y = 0; y < picture.height(); y++) {
                energyTo[x][y] = energy(x, y);
            }
        }
    }

    private void setPixelsFrom(double[][] energyTo, Pixel[][] pixelFrom, int x, int y) {
        List<Pixel> pixelAncestors = adj(x, y);
        pixelFrom[x][y] = pixelAncestors.isEmpty() ? null : minEnergyPixel(pixelAncestors, energyTo);
    }

    private List<Pixel> adj(int x, int y) {
        List<Pixel> results = new ArrayList<>();
        if (x == 0)
            return results;
        if (y > 0)
            results.add(new Pixel(x - 1, y - 1));
        if (y < picture.height() - 1)
            results.add(new Pixel(x - 1, y + 1));
        results.add(new Pixel(x - 1, y));
        return results;
    }

    private Pixel minEnergyPixel(Iterable<Pixel> results, double[][] energyTo) {
        double min = Double.POSITIVE_INFINITY;
        Pixel r = null;
        for (Pixel result : results) {
            if (energyTo[result.x][result.y] < min) {
                min = energyTo[result.x][result.y];
                r = result;
            }
        }
        return r;
    }

    private void updateEnergiesWithEnergiesTo(double[][] energyTo, Pixel[][] pixelFrom,
                                              int x, int y) {
        energyTo[x][y] = pixelFrom[x][y] == null ? 1000 : energyTo[x][y] +
                energyTo[pixelFrom[x][y].x][pixelFrom[x][y].y];
    }

    private Pixel getLastPixel(double[][] energyTo, Pixel[][] pixelFrom) {
        if (pixelFrom.length == 1) {
            return new Pixel(0, 0);
        }
        double min = Double.POSITIVE_INFINITY;
        int r = -1;
        for (int i = 0; i < height(); i++) {
            int x = pixelFrom[width() - 1][i].x;
            int y = pixelFrom[width() - 1][i].y;
            if (energyTo[x][y] < min) {
                min = energyTo[x][y];
                r = y;
            }
        }
        return new Pixel(width() - 1, r);
    }

    private int[] getSeam(Pixel lastPixel, Pixel[][] pixelFrom) {
        int[] seam = new int[width()];
        Pixel pixel = lastPixel;
        int counter = width() - 1;
        while (pixel != null) {
            seam[counter] = pixel.y;
            pixel = pixelFrom[pixel.x][pixel.y];
            counter--;
        }
        return seam;
    }

    // sequence of indices for vertical seam
    public int[] findVerticalSeam() {
        if (!isTransposed) {
            transposePicture();
        }
        int[] results = findHorizontalSeam();
        if (isTransposed) {
            transposePicture();
        }
        return results;
    }

    private void transposePicture() {
        Picture tempPicture = new Picture(height(), width());

        for (int i = 0; i < width(); i++) {
            for (int j = 0; j < height(); j++) {
                tempPicture.setRGB(j, i, picture.getRGB(i, j));
            }
        }
        picture = tempPicture;
        isTransposed = !isTransposed;
    }

    // remove horizontal seam from current picture
    public void removeHorizontalSeam(int[] seam) {
        if (seam == null) {
            throw new IllegalArgumentException();
        }
        if (!isTransposed) {
            transposePicture();
        }
        removeVerticalSeam(seam);
        if (isTransposed) {
            transposePicture();
        }
    }

    // remove vertical seam from current picture
    public void removeVerticalSeam(int[] seam) {
        isSeamValid(seam);
        isPictureToNarrow();

        int[][] tempArray = getResizedPhotoPixelsArray(seam);

        Picture resizedPicture = new Picture(width() - 1, height());
        for (int i = 0; i < resizedPicture.width(); i++) {
            for (int j = 0; j < resizedPicture.height(); j++) {
                resizedPicture.setRGB(i, j, tempArray[j][i]);
            }
        }
        picture = resizedPicture;

    }

    private int[][] getResizedPhotoPixelsArray(int[] seam) {
        int[][] pictureArray = getRGBArrayFromPicture();
        int[][] tempArray = new int[height()][width() - 1];
        for (int i = 0; i < height(); i++) {
            int fal = seam[i];
            int sal = width() - 1 - seam[i];
            System.arraycopy(pictureArray[i], 0, tempArray[i], 0, fal);
            System.arraycopy(pictureArray[i], seam[i] + 1, tempArray[i], seam[i], sal);
        }
        return tempArray;
    }

    private void isPictureToNarrow() {
        if (picture.width() < 2)
            throw new IllegalArgumentException();
    }

    private void isSeamValid(int[] seam) {
        if (seam == null || seam.length == 0)
            throw new IllegalArgumentException();
        if (seam.length != height())
            throw new IllegalArgumentException();
        if (seam[0] < 0 || seam[0] >= width()) {
            throw new IllegalArgumentException();
        }
        if (seam.length > 1) {
            int a = 1;
            while (a < seam.length) {
                if (seam[a] < 0 || seam[a] >= width()) {
                    throw new IllegalArgumentException();
                }
                if (seam[a] - seam[a - 1] < -1 || seam[a] - seam[a - 1] > 1) {
                    throw new IllegalArgumentException();
                }
                a++;
            }
        }
    }

    private int[][] getRGBArrayFromPicture() {
        int[][] tempArray = new int[height()][width()];
        for (int i = 0; i < height(); i++) {
            for (int j = 0; j < width(); j++) {
                tempArray[i][j] = picture.getRGB(j, i);
            }
        }
        return tempArray;
    }

    //  unit testing (optional)
    public static void main(String[] args) {
        Picture picture = new Picture(args[0]);
        SeamCarver sc = new SeamCarver(picture);
        sc.removeVerticalSeam(new int[] { -1 });
    }

    private class Pixel {
        private final int x;
        private final int y;

        public Pixel(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public String toString() {
            return " (" + x +
                    ", " + y + ") ";
        }
    }

}