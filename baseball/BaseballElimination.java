/* *****************************************************************************
 *  Name:
 *  Date:
 *  Description:
 **************************************************************************** */

import edu.princeton.cs.algs4.FlowEdge;
import edu.princeton.cs.algs4.FlowNetwork;
import edu.princeton.cs.algs4.FordFulkerson;
import edu.princeton.cs.algs4.In;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class BaseballElimination {
    private final Map<String, Integer> teams;
    private final int numberOfTeams;
    private final int[][] teamStats; // won[0], lost[1], left[2]
    private final int[][] gamesToPlay;
    private int capacity;

    // create a baseball division from given filename in format specified below
    public BaseballElimination(String filename) {
        In in = new In(filename);
        teams = new HashMap<>();
        numberOfTeams = Integer.parseInt(in.readLine());
        teamStats = new int[numberOfTeams][3];
        gamesToPlay = new int[numberOfTeams][numberOfTeams];
        for (int i = 0; i < numberOfTeams; i++) {
            String[] teamData = in.readLine().trim().split("\\s+");
            teams.put(teamData[0], i);
            for (int j = 1; j < 4; j++) {
                teamStats[i][j - 1] = Integer.parseInt(teamData[j]);
            }
            for (int j = 4; j < teamData.length; j++) {
                gamesToPlay[i][j - 4] = Integer.parseInt(teamData[j]);
            }
        }
    }

    // number of teams
    public int numberOfTeams() {
        return numberOfTeams;
    }

    // all teams
    public Iterable<String> teams() {
        return teams.keySet();
    }

    // number of wins for given team
    public int wins(String team) {
        int index = getTeamIndex(team);
        return teamStats[index][0];
    }

    // number of losses for given team
    public int losses(String team) {
        int index = getTeamIndex(team);
        return teamStats[index][1];
    }

    // number of remaining games for given team
    public int remaining(String team) {
        int index = getTeamIndex(team);
        return teamStats[index][2];
    }

    private int getTeamIndex(String team) {
        isValidTeam(team);
        return teams.get(team);
    }

    // number of remaining games between team1 and team2
    public int against(String team1,
                       String team2) {
        int index1 = getTeamIndex(team1);
        int index2 = getTeamIndex(team2);
        return gamesToPlay[index1][index2];
    }

    // is given team eliminated?
    public boolean isEliminated(String team) {
        // trivial
        if (wins(team) + remaining(team) < maxWins()) {
            return true;
        }
        // non-trivial
        FordFulkerson ff = getFord(team);
        if (ff.value() == capacity) {
            return false;
        }
        return true;
    }

    private FordFulkerson getFord(String team) {
        int numberOfGames = (numberOfTeams - 1) * numberOfTeams / 2;
        int s = numberOfGames + numberOfTeams;
        int t = numberOfGames + numberOfTeams + 1;
        FlowNetwork fn = new FlowNetwork(2 + numberOfTeams + numberOfGames);
        int vertex = 0;
        capacity = 0;

        for (int i = 0; i < numberOfTeams; i++) {
            fn.addEdge(new FlowEdge(numberOfGames + i, t,
                                    wins(team) + remaining(team) - teamStats[i][0]));
            for (int j = i + 1; j < numberOfTeams; j++) {
                fn.addEdge(new FlowEdge(vertex, numberOfGames + i, Double.POSITIVE_INFINITY));
                fn.addEdge(new FlowEdge(vertex, numberOfGames + j, Double.POSITIVE_INFINITY));
                fn.addEdge(new FlowEdge(s, vertex, gamesToPlay[i][j]));
                vertex++;
                capacity += gamesToPlay[i][j];
            }
        }
        return new FordFulkerson(fn, s, t);
    }

    // subset R of teams that eliminates given team; null if not eliminated
    public Iterable<String> certificateOfElimination(String team) {
        // trivial
        if (wins(team) + remaining(team) < maxWins()) {
            return moreWinsThan(team);
        }
        // nonTrivial
        FordFulkerson ff = getFord(team);
        if (ff.value() == capacity) {
            return null;
        }
        return moreWinsThan(ff);
    }

    private Iterable<String> moreWinsThan(String team) {
        Set<String> teamsWithMoreWins = new HashSet<>();
        int sum = wins(team) + remaining(team);
        for (String name : teams()) {
            if (sum < wins(name)) {
                teamsWithMoreWins.add(name);
            }
        }
        return teamsWithMoreWins;
    }

    private Iterable<String> moreWinsThan(FordFulkerson ff) {
        int numberOfGames = (numberOfTeams - 1) * numberOfTeams / 2;
        Set<String> teamsWithMoreWins = new HashSet<>();
        for (String name : teams()) {
            if (ff.inCut(teams.get(name) + numberOfGames)) {
                teamsWithMoreWins.add(name);
            }
        }
        return teamsWithMoreWins;
    }

    private int maxWins() {
        int max = -1;
        for (int i = 0; i < numberOfTeams; i++) {
            max = Math.max(max, teamStats[i][0]);
        }
        return max;
    }

    private void isValidTeam(String team) {
        if (!teams.containsKey(team)) {
            throw new IllegalArgumentException();
        }
    }

    public static void main(String[] args) {
        BaseballElimination be = new BaseballElimination("teams60.txt");
        for (int i = 0; i < 36; i++) {
            System.out.println(be.isEliminated("Team" + i));
            System.out.println(be.certificateOfElimination("Team" + i));
        }
    }
}
